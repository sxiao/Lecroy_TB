__doc__ = "Read data from infiniium oscilloscope, recorded with get_allseg.c"
__author__ = "Bruno Lenzi"
__date__ = "Oct 2016"

import os, numpy as np, itertools, unittest, time
from itertools import tee, izip

def splitseq(seq,size):
  " Split up seq in pieces of size "
  return [seq[i:i+size] for i in range(0, len(seq), size)]

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)


def getNchannels(txtFile):
  """Return the number of active channels and scopes from txt file using the time and
   oscilloscope name information"""
  for iline, line in enumerate(open(txtFile)):
    lineSplit = line.split(',')
    t0, osc = lineSplit[16], lineSplit[17]
    print t0,osc,"===="
    try:
      if osc == oscRef and t0 != t0ref:
        print iline,len(scopes),"ooooo"
        return iline, len(scopes)
      elif osc != oscRef:
        scopes.add(osc)
    except NameError:
      t0ref = t0
      oscRef = osc
      scopes = set([osc])
  # Reached the end of the file
  print iline + 1, len(scopes),"******"
  return iline + 1, len(scopes)

datapath = '~/eos/atlas/atlascerngroupdisk/det-hgtd/testbeam/Oct2016'

class DataReader:
  """Class to read data recorded via get_allseg.c"""
  def __init__(self, file, doSummary = True):
    try:
      timestamp = int(file) # timestamp given instead of filename
      basepath = os.path.expanduser(datapath)
      oscPattern = 'data_{0}'
      filename = os.path.join(basepath, oscPattern.format(timestamp))
    except ValueError:
      filename, extension = os.path.splitext(file)
      if extension not in ['', '.txt', '.dat']:
        raise IOError('Invalid extension: %s' % extension)

    self.txtFile = filename + '.txt'
    self.datFile = filename + '.dat'
    if not all(map(os.path.isfile, (self.txtFile, self.datFile))):
      raise IOError('Could not find txt and/or dat files ({0}, {1}'.format(self.txtFile, self.datFile))

    self.txtBytesRead = 0 # bytes read in txt file
    self.Nchannels, self.Nscopes = getNchannels(self.txtFile)
    print self.Nchannels, self.Nscopes,"====="

    self.info = []
    self.cycleInfo = []
    self.evtsPerCycle = []
    self.readTxt()
    self.cyclesRead = 0
    self.doSummary = doSummary
    self.dataSummary = None

  
  def readTxt(self):
    "readTxt(self, update=False) -> read txt file and update information in self.info"
    if os.path.getsize(self.txtFile) - self.txtBytesRead < 220 * self.Nchannels:
      return False
    with open(self.txtFile) as f:
      import ast
#       f.seek(self.txtBytesRead)
#       newInfo = map(ast.literal_eval, f)
      lines = f.readlines()
      newInfo = map(ast.literal_eval, lines[len(self.evtsPerCycle)*self.Nchannels:])
      if len(newInfo) < self.Nchannels:
        return False
        raise IOError('Only read {0} lines of txt file instead of {1}'.format(len(newInfo), self.Nchannels))
      newCycleInfo = self.getCycleInfo(newInfo)
      if newCycleInfo is None: return False        
#         return self.readTxt()
      self.txtBytesRead = f.tell() # position in file
    
    self.info.extend( newInfo )
    self.evtsPerCycle.extend( [i['Nevts'] for i in newCycleInfo] )
    self.cycleInfo.extend( newCycleInfo )
    return True
  
  def getCycleInfo(self, info):
    """Given a list of info (metadata), return a list of dictionaries with information
    corresponding to each cycle"""
    fields = dict(Nsamples=2, deltaT=4, T0=5, deltaV=7, V0=8, Nevts=24)
    singleFields = ['Nevts']
#     singleFields = 'Nsamples', 'Nevts'
    arrayFields = 'deltaT', 'T0', 'deltaV', 'V0', 'Nsamples'
    # Group info lists by cycle and create a nested list (cycle, channel)
    nestedList = [zip(*i) for i in splitseq(info, self.Nchannels)]
    #nestedList = [np.array(i, dtype=object).T for i in splitseq(info, self.Nchannels)]
    # Convert desired fields to a dictionary for each cycle
    infoDict = [dict( (key, values[index]) for key, index in fields.iteritems() ) for values in nestedList]
    # Check single fields and replace list by single value
    # Append bytes per cycle
    for d in infoDict:
      for key, values in d.iteritems():
        if len(values) != self.Nchannels or \
          (key in singleFields and not all(i == values[0] for i in values)):
            return None
        if key in singleFields:
          d[key] = values[0]
#           v = values[0]
#           if not all(i == v for i in values):
#             raise ValueError('Different values for {key}: {v}'.format(key=key, v=values))
#           d[key] = v
#         elif len(values) != self.Nchannels:
#           return None
# #           raise ValueError('Length of {key}: {v}, instead of {n}'.format(key=key, v=len(values), n=self.Nchannels))
        if key in arrayFields:
          d[key] = np.array(values)
      # Bytes (ADC values + times): Nevts * (2 * sum of samples + 8)
      # 16 bits for ADCs, 64 bits for times
      d['bytes'] = d['Nevts'] * (2 * sum(d['Nsamples']) + 8 * self.Nscopes )
      # Convert to array
    
    return infoDict    
  
  def readCycle(self, cycle = -1):
    """readCycle(self, cycle = -1) -> Read data from next (or given) cycle and save it in
     self.pulses, self.times"""
    # make sure there are new cycles to be read and txt file is up to date
    if cycle == -1 and self.cyclesRead == len(self.cycleInfo) and not self.readTxt(): 
      return False # No more cycles to be read
    # Determine the number of bytes to skip to reach the desired cycle
    if cycle == -1: 
      cycle = self.cyclesRead # there might be more cycles in cycleInfo than the ones read
    bytesRead = sum(i['bytes'] for i in self.cycleInfo[:cycle])
    
    Nchannels = self.Nchannels
    cycleInfo = self.cycleInfo[cycle]
    fields = 'Nsamples', 'Nevts', 'deltaV', 'V0', 'T0', 'deltaT'
    Nsamples, Nevts, deltaV, V0 ,T0, deltaT = map(cycleInfo.__getitem__, fields)
    Nvalues = Nevts * sum(Nsamples)
    
    if os.path.getsize(self.datFile) < bytesRead + self.cycleInfo[cycle]['bytes']:
      # Not enough bytes to read. Wait and try again
      time.sleep(0.2)
      return self.readCycle(cycle)
    with open(self.datFile, 'rb') as f:
      f.seek(bytesRead)
      
      # Read ADC values
      x = np.fromfile(f, count=Nvalues, dtype=np.int16)
      if len(x) != Nvalues: 
        raise ValueError('Only read %s ADC values instead of %s' % (len(x), Nvalues))
      
      # Read times
      self.times = np.fromfile(f, count=Nevts*self.Nscopes, dtype=np.float64)
      if len(self.times) != Nevts*self.Nscopes:
        raise ValueError('Only read %s time values instead of %s' % (len(self.times), Nevts*self.Nscopes))      
      
      if np.all(Nsamples[0] == Nsamples): # use np.array if possible
        self.ADC = x.reshape( Nchannels, Nevts, Nsamples[0] )
      else:
        indices = pairwise([0] + list(Nevts * Nsamples.cumsum()))
        self.ADC = [x[i:j].reshape(Nevts, k) for (i,j),k in zip(indices, Nsamples)]

    # Convert ADC to V
    if isinstance(self.ADC, np.ndarray):
      self.pulses = self.ADC * deltaV[:, np.newaxis, np.newaxis] + V0[:, np.newaxis, np.newaxis]
    else:
      self.pulses = [i * j + k for i,j,k in izip(self.ADC, deltaV, V0)]
    
    # Time of each sample relative to trigger
    self.t = [t0 + dt * np.arange(n) for t0,dt,n in izip(T0, deltaT, Nsamples)]

    if cycle == self.cyclesRead: # reading cycles in sequence
      self.cyclesRead += 1
    if self.doSummary:
      self.dataSummary = DataSummary(self, self.dataSummary)
    return True
        
  def append(self, pulses, times):
    "Append pulses, times, info to existing objects"
    self.pulses = np.concatenate( [self.pulses, pulses], axis=1)
    self.times = np.concatenate( [self.times, times] )
  
  def readAllCycles(self):
    "Read all cycles and store, pulses, times and info (metadata)"
    pulses, times = izip(*(self.readCycle() for i,j in enumerate(self.cycleInfo)))
    self.pulses = np.concatenate( pulses, axis=1)
    self.times = np.concatenate( times )


class DataSummary:
  "Class to summarise data from one or multiple cycles"
  level = 0
  def __init__(self, reader, ds=None, threshold=-0.025):
    import random
    pulses = reader.pulses
    
    self.randomPulses = [i[random.randrange(0, i.shape[0])] for i in pulses]
    self.meanPulses = [i.mean(axis=0) for i in pulses]

    self.peakV = np.array([i.min(axis=1) for i in pulses])
    self.peakI = np.array([i.argmin(axis=1) for i in pulses])
    self.NpeaksAboveTh = np.sum(self.peakV < threshold, axis=1)

    self.Nevts = reader.cycleInfo[reader.cyclesRead-1]['Nevts'] # counter already updated
    self.evtsPerCycle = [self.Nevts]
    self.meanPulsesAll = self.meanPulses
    if ds is not None: # merge info with previous cycles
      self.meanPulsesAll = [(dsMean * ds.Nevts + mean * self.Nevts) / (ds.Nevts + self.Nevts) for dsMean, mean in zip(ds.meanPulsesAll, self.meanPulsesAll)]
      self.Nevts += ds.Nevts
      self.evtsPerCycle.extend( ds.evtsPerCycle )
      self.NpeaksAboveTh += ds.NpeaksAboveTh
      self.peakV = np.concatenate( [self.peakV, ds.peakV], axis=1 )
      self.peakI = np.concatenate( [self.peakI, ds.peakI], axis=1 )


class TestDataReader(unittest.TestCase):
  "Class to test DataReader"
  def setUp(self):
    "Dump some signals to a file"
    self.ADC = np.array([
       25216, 25036, 25116, 25216, 25496, 25196, 25344, 25200, 25316,
       25264, 25228, 25064, 25024, 24880, 25172, 25436, 25088, 25300,
       25072, 25124, 24964, 24924, 25444, 25172, 25208, 25260, 25128,
       25480, 25256, 25536, 25780, 25660, 25520, 25456, 25396, 25004,
       25572, 25400, 25400, 25408, 23804,  2604,  6400, 12316, 17784,
       21820, 24044, 24760, 25492, 25820, 25588, 25292, 25292, 25692,
       26192, 25884, 25940, 25588, 25200, 24928, 25456, 25488, 25708,
       25728, 25604, 26012, 26128, 25920, 25792, 25644, 25320, 25524,
       25436, 25332, 25476, 25844, 25580, 25596, 25492, 25364, 25636], dtype=np.int16)
    
    self.Nsamples = len(self.ADC)
    self.Ncycles, self.Nevts, self.Nchannels = 5, 10, 3
    self.times = np.linspace(0, 3, self.Nevts, dtype=np.float64)
    
    self._info = '4,1,{samples},1,2.5000000E-11,{time},0E+00,1.31732E-05,-3.33754E-01,0E+00,2,5.00000E-08,-2.5000000000E-08,8.00000E-01,-3.45400E-01,"28 OCT 2016","02:41:10:73","DSO91204A:MY55300102",2,100,2,1,2.00000E+09,0E+00,{evts}'    
    
    self.txtFile, self.datFile = '/tmp/testReader.txt', '/tmp/testReader.dat'
    os.remove(self.txtFile)
    os.remove(self.datFile)
    for i in xrange(self.Ncycles):
      self.info = self._info.format(samples=self.Nsamples, 
        evts=self.Nevts, time=np.random.rand())      
      self.writeCycle()
  
  def runTest(self): pass
    
  def writeCycle(self):
    "Write cycle to files"
    with open(self.datFile, 'ab') as f:
      for evt in xrange(self.Nevts):
        for channel in xrange(self.Nchannels):
          f.write( self.ADC.tobytes() )
      for evt in xrange(self.Nevts):
        f.write( self.times.tobytes() )
    with open(self.txtFile, 'a') as f:
      for i in xrange(self.Nchannels):
        f.write( self.info + '\n' )    
    
  def test_read_cycle(self):
    r = DataReader(self.txtFile)
    r.readCycle()
    self.assertEqual( r.pulses.shape, (self.Nchannels, self.Nevts, self.Nsamples) )
    self.assertTrue( np.all(self.ADC == r.ADCs[0,0]) )
    self.assertEqual( r.times.shape, (self.Nevts,) )    
    self.assertTrue( np.all(self.times == r.times) )

  def test_read_more(self):
    r = DataReader(self.txtFile)
    for i in xrange(self.Ncycles):
      r.readCycle()
      self.assertEqual( r.pulses.shape, (self.Nchannels, self.Nevts, self.Nsamples) )
      self.assertEqual( r.times.shape, (self.Nevts,) )
      print r.ADCs[0,0]
      self.assertTrue( np.all(self.ADC == r.ADCs[0,0]) )




if __name__ == '__main__':
  import user, sys
  if len(sys.argv) > 1:
    inputfile = sys.argv[1]
    r = DataReader(inputfile)
  else:
    t = TestDataReader()
